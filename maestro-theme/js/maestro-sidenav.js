// https://www.w3schools.com/howto/howto_js_sidenav.asp

function openNav() {
	// document.getElementById("mySidenav").style.display = "table-cell";
	document.getElementById("mySidenav").style.width = "250px";
	document.getElementById("mainSideNav").style.marginLeft = "250px";
	// console.log(document.getElementById("mainSideNav").offsetWidth);
}

function closeNav() {
	// document.getElementById("mySidenav").style.display = "none";
	document.getElementById("mySidenav").style.width = "0px";
	document.getElementById("mainSideNav").style.marginLeft = "0px";
}